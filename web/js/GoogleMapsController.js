/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 Created on : Oct 23, 2014, 6:20:21 PM
 Author     : Pedro
 */

function GoogleMapsController(google) {

    this.safetyAPI = new SafetyAPI();
    this.crimeIds = new HashSet();
    this.crimes = [];
    this.share = false;
    this.route = false;
    this.searchRadius = 0;
    this.heatMapRadius = 0;
    this.screenSize = Math.sqrt(Math.pow(screen.height, 2) + Math.pow(screen.width, 2));
    this.google = google;
    this.initialize();
}

GoogleMapsController.prototype.shareCrime = function(location, description, epoch, period) {

    var self = this;
    this.safetyAPI.uploadCrime(location.lat(), location.lng(), description, epoch, period, function(data) {

        self.share = false;
        if (data.result === "success") {
            var currentPeriod = "";
            var hour = new Date().getHours();
            if (hour >= 5 && hour < 12) {
                currentPeriod = "Morning";
            } else if (hour >= 12 && hour < 18) {
                currentPeriod = "Afternoon";
            } else {
                currentPeriod = "Evening";
            }
            if (currentPeriod === period) {
                self.placeMarker(data.crimeId, location, description, epoch, period);
            } else {
                information('Crime compartilhado com sucesso! Obrigado.');
            }
        } else if (data.result === "failed") {
            information("Desculpe, não foi possível realizar esta operação. Dados inválidos.");
        }
    });
};

GoogleMapsController.prototype.getCrimes = function(location) {

    var self = this;
    this.safetyAPI.requestCrime(location.lat(), location.lng(), (this.searchRadius / 1000.0), function(data) {

        for (var i = 0; i < data.points.length; i++) {
            var crimeId = data.points[i].crimeId;
            var point = new self.google.maps.LatLng(data.points[i].latitude, data.points[i].longitude);
            var description = data.points[i].description;
            var epoch = data.points[i].epoch;
            var period = data.points[i].period;
            self.placeMarker(crimeId, point, description, epoch, period);
        }
    });
};

GoogleMapsController.prototype.placeMarker = function(crimeId, location, description, epoch, period) {

    if (!this.crimeIds.contains(crimeId)) {
        var marker = new this.google.maps.Marker({
            position: location,
            map: this.map
        });

        var infowindow = new this.google.maps.InfoWindow({
            content: crimeContent(description, epoch, period)
        });

        var self = this;
        this.google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(self.map, marker);
        });
        this.crimes.push(location);
        this.crimeIds.add(crimeId);
        this.heatmap.setData(this.crimes);
    }
};

GoogleMapsController.prototype.showHeatMap = function() {
    this.heatmap.getMap() === null ? this.heatmap.setMap(this.map) : this.heatmap.setMap(null);
};

GoogleMapsController.prototype.updateRadius = function() {

    var bounds = this.map.getBounds();
    var distance = this.google.maps.geometry.spherical.computeDistanceBetween(bounds.getNorthEast(), bounds.getSouthWest());

    //SearchRadius is 25% of screen size in meters.
    this.searchRadius = distance / 4.0;

    //HeatMap has "real radius" of 200 meters.
    this.heatMapRadius = (200 * this.screenSize) / distance;

    var flag = this.heatmap.getMap() === null ? false : true;
    this.heatmap.setMap(null);
    this.heatmap.set('radius', this.heatMapRadius);
    if (flag) {
        this.heatmap.setMap(this.map);
    }
};

GoogleMapsController.prototype.wrapp = function(location) {

    var wrapped = new this.google.maps.LatLng(location.lat(), location.lng());
    return wrapped;
};

GoogleMapsController.prototype.geocoding = function(address, callback) {

    var self = this;

    var bounds = this.map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    var newNE = new this.google.maps.LatLng(ne.lat() + 0.3, ne.lng() + 0.3);
    var newSW = new this.google.maps.LatLng(sw.lat() - 0.3, sw.lng() - 0.3);
    var newBounds = new this.google.maps.LatLngBounds(newSW, newNE);

    this.geocoder.geocode({
        address: address,
        bounds: newBounds
    }, function(results, status) {
        if (status === self.google.maps.GeocoderStatus.OK) {
            callback && callback.call(self, results[0].geometry.location);
        } else {
            information("Infelizmente não conseguimos encontrar este endereço.");
        }
    });
};

GoogleMapsController.prototype.reverseGeocoding = function(location) {

    var self = this;

    this.geocoder.geocode({
        latLng: location
    }, function(results, status) {
        if (status === self.google.maps.GeocoderStatus.OK) {
            return results[0].formatted_address;
        } else {
            information("Infelizmente não conseguimos encontrar este endereço.");
        }
    });
};

GoogleMapsController.prototype.calculateRoute = function(destination) {

    var self = this;
    var pos = this.userPosition.getPosition();
    this.safetyAPI.selectRoute(pos.lat(), pos.lng(), destination.lat(), destination.lng(), function(data) {

        var points = [];
        var waypoints = [];
        var nWay = data.nWay;

        for (var i = 0; i < data.points.length; i++) {
            var point = new self.google.maps.LatLng(data.points[i].latitude, data.points[i].longitude);
            points.push(point);

            waypoints.push({
                location: point,
                stopover: true
            });
        }
        //self.myPath.setPath(points);

        var request = {
            origin: self.userPosition.getPosition(),
            destination: destination,
            travelMode: self.google.maps.TravelMode.WALKING,
            waypoints: waypoints,
            optimizeWaypoints: true
        };

        self.directionsService.route(request, function(result, status) {
            if (status === self.google.maps.DirectionsStatus.OK) {

                var path = [];
                var routes = result.routes;
                for (var i = 0; i < routes.length; i++) {
                    var route = routes[i];
                    var legs = route.legs;
                    for (var j = 0; j < legs.length; j++) {
                        var leg = legs[j];
                        var steps = leg.steps;
                        for (var k = 0; k < steps.length; k++) {
                            var step = steps[k];
                            var p = step.path;
                            for (var l = 0; l < p.length; l++) {
                                path.push(p[l]);
                            }
                        }
                    }
                }
                //self.secondPath.setPath(path);

                var unique = [];
                for (var i = 0; i < path.length; i++) {
                    var count = 0;
                    for (var j = 0; j < path.length; j++) {
                        if (path[i].lng() === path[j].lng() && path[i].lat() === path[j].lat()) {
                            count = count + 1;
                        }
                    }
                    if (count === 1) {
                        unique.push(path[i]);
                    }
                }

                var waypoints = [];
                var m = Math.floor(unique.length / nWay);
                for (var i = 0; i < unique.length; i++) {
                    var point = new self.google.maps.LatLng(unique[i].lat(), unique[i].lng());
                    if (i !== 0 && i % m === 0 && waypoints.length < nWay - 1) {
                        waypoints.push({
                            location: point,
                            stopover: true
                        });
                    }
                }

                var request = {
                    origin: self.userPosition.getPosition(),
                    destination: destination,
                    travelMode: self.google.maps.TravelMode.WALKING,
                    waypoints: waypoints,
                    optimizeWaypoints: true
                };

                self.directionsService.route(request, function(result, status) {
                    if (status === self.google.maps.DirectionsStatus.OK) {
                        //console.log(result);
                        self.directionsRenderer.setDirections(result);
                        self.directionsRenderer.setMap(self.map);
                    }
                });
            }
        });
    });
    this.route = false;
};

GoogleMapsController.prototype.calculateDanger = function() {

    var self = this;
    var yourPosition = this.userPosition.getPosition();
    var array = [];
    var dangerRadius = 1000, dangerLevel = 0, distance = 0, weight = 0.5;

    this.crimes.forEach(function(crime) {

        distance =
                self.google.maps.geometry.spherical.computeDistanceBetween(yourPosition, crime);

        if (!isNaN(distance) && distance < dangerRadius) {
            array.push((dangerRadius - distance) / dangerRadius);
        }
    });

    array.sort().reverse().forEach(function(distance) {
        dangerLevel += weight * distance;
        weight = weight / 2;
    });

    return dangerLevel;
};

GoogleMapsController.prototype.initialize = function() {

    var self = this;

    /**
     * New map object ...
     */
    var mapOptions = {
        zoom: 15,
        minZoom: 13,
        maxZoom: 18,
        zoomControlOptions: {position: this.google.maps.ControlPosition.LEFT_CENTER},
        panControlOptions: {position: this.google.maps.ControlPosition.LEFT_CENTER},
        streetViewControl: false,
        mapTypeControl: false
    };
    this.map = new this.google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    /*this.myPath = new this.google.maps.Polyline({
     path: [],
     strokeColor: "#FF0000",
     strokeOpacity: 1.0,
     strokeWeight: 2
     });
     this.myPath.setMap(this.map);
     
     this.secondPath = new this.google.maps.Polyline({
     path: [],
     strokeColor: "#FF00EE",
     strokeOpacity: 1.0,
     strokeWeight: 5
     });
     this.secondPath.setMap(this.map);*/

    /**
     * Directions handlers
     */
    this.directionsRenderer = new this.google.maps.DirectionsRenderer({
        preserveViewport: true
    });
    this.directionsService = new this.google.maps.DirectionsService();

    /**
     * Geocoder handler
     */
    this.geocoder = new this.google.maps.Geocoder();


    /**
     * Try to detect user's geo_location ....
     */
    var initialLocation;
    if (window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(function(position) {
            initialLocation = new self.google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            self.map.setCenter(initialLocation);
        }, function() {
            window.navigator.geolocation.getCurrentPosition(function(position) {
                initialLocation = new self.google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                self.map.setCenter(initialLocation);
            }, function(error) {
                information("Por favor, ative o sistema de geo-localização e tente novamente: " + error.message);
            }, {enableHighAccuracy: false, timeout: 120000, maximumAge: 1000 * 3600});
        }, {enableHighAccuracy: true, timeout: 120000, maximumAge: 1000 * 3600});
    } else {
        information("Seu dispositivo não suporta geo-localização.");
    }

    /**
     * Add a invisible heatmap to the map
     */
    this.heatmap = new this.google.maps.visualization.HeatmapLayer({
        opacity: 0.3,
        map: null
    });

    /**
     * User Position Marker
     */
    this.userPosition = new UserPosition(this.google, this.map);


    /**
     * Map's event listeners ...
     */

    //On click
    this.google.maps.event.addListener(this.map, 'click', function(event) {
        //Execute every time the user click on the map ...
        if (self.share === true) {
            displayCrimeDialogBox.call(self, event.latLng);
        } else if (self.route === true) {
            self.calculateRoute(event.latLng);
        }
    });

    var flag = true;
    //On center changed
    this.google.maps.event.addListener(this.map, 'center_changed', function() {
        //Execute every time the map center change ...
        if (flag) {
            flag = false;
            setTimeout(function() {
                self.getCrimes(self.wrapp(self.map.getCenter()));
                flag = true;
            }, 1000);
        }
    });

    //On zoom changed ...
    this.google.maps.event.addListener(this.map, 'zoom_changed', function() {
        //Execute every time the zoom change ...
        self.updateRadius();
        self.getCrimes(self.wrapp(self.map.getCenter()));
    });

    //On load ...
    this.loaded = false;
    this.google.maps.event.addListener(this.map, 'tilesloaded', function() {
        //Execute only once, when map load at first time in the screen ...
        if (!self.loaded) {
            self.loaded = true;

            var updateRange = function() {
                var danger = self.calculateDanger();
                if (!isNaN(danger)) {
                    document.getElementById('range').value = danger * 100;
                }
            };

            setTimeout(function() {
                self.updateRadius();
                var mapCenter = self.wrapp(self.map.getCenter());
                self.userPosition.setPosition(mapCenter);
                self.userPosition.updatePosition();
                self.getCrimes(mapCenter);
                setTimeout(updateRange, 2000);
            }, 1000);

            setInterval(updateRange, 60000);
        }
    });


    /*
     * Button Handlers
     */
    document.getElementById("btShareCrime").onclick = function() {
        displayAddressDialogBox(function() {
            self.share = true;
        }, function(address) {
            self.geocoding(address, displayCrimeDialogBox);
        }, 'Nós sentimos muito pelo o que ocorreu, somos gratos por você compartilhar.');
    };
    document.getElementById("btHeatMap").onclick = function() {
        self.showHeatMap();
    };
    document.getElementById("btRoute").onclick = function() {
        self.directionsRenderer.setMap(null);
        displayAddressDialogBox(function() {
            self.route = true;
        }, function(address) {
            self.geocoding(address, self.calculateRoute);
        }, 'Por favor, digite o endereço ou clique no mapa. Ideal para trajetos a pé.');
    };
};

/*
 * Main Method
 */
function main() {
    new GoogleMapsController(google);
}

function loadScript() {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.googleapis.com/maps/api/js?libraries=visualization,geometry&callback=main";
    document.body.appendChild(script);
}
window.onload = loadScript;