/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function UserPosition(google, map) {

    this.google = google;
    this.map = map;
    this.marker = new google.maps.Marker({
        map: map,
        icon: {
            url: '../img/gpsloc.png',
            size: new google.maps.Size(34, 34),
            scaledSize: new google.maps.Size(17, 17),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(8, 8)
        },
        zIndex: 2
    });
}

UserPosition.prototype.setPosition = function(position) {
    this.marker.setPosition(position);
};

UserPosition.prototype.getPosition = function() {
    return this.marker.getPosition();
};

UserPosition.prototype.updatePosition = function() {
    var self = this;
    if (window.navigator.geolocation) {
        window.navigator.geolocation.watchPosition(function(position) {
            self.setPosition(new self.google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        });
    }
};

