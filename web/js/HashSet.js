/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 Created on : Nov 1, 2014, 6:20:21 PM
 Author     : Pedro
 
 */


function HashSet() {
    this.hash = {};
}

HashSet.prototype.add = function(key) {
    this.hash[key] = true;
};

HashSet.prototype.remove = function(key) {
    delete this.hash[key];
};

HashSet.prototype.contains = function(key) {
    return (this.hash[key] === true);
};