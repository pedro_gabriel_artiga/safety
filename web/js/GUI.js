/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 Crime Dialog Box 
 **/
function displayCrimeDialogBox(location) {

    var today = new Date();

    var dialogBox = '<div id="crime_modal" class="overlay">';
    dialogBox += '<div id="crime_modal_div">';
    dialogBox += '<div id="crime_modal_message">';
    dialogBox += 'Quase pronto, faltam só mais algumas informações.';
    dialogBox += '</div>';
    dialogBox += '<div id="crime_modal_select">';
    dialogBox += '<select id="crime_modal_description" class="round_border">';
    dialogBox += '<option>Descrição</option>';
    dialogBox += '<option value="Robbery">Assalto</option>';
    dialogBox += '<option value="Attack">Agressão Física</option>';
    dialogBox += '<option value="Insult">Agressão Psicológica</option>';
    dialogBox += '<option value="Harassment">Assédio Sexual</option>';
    dialogBox += '<option value="Theft">Furto</option>';
    dialogBox += '<option value="Racism">Ódio Racial</option>';
    dialogBox += '<option value="Gender Violence">Violência de Gênero</option>';
    dialogBox += '</select>';
    dialogBox += daySelector(today);
    dialogBox += monthSelector();
    dialogBox += yearSelector(today);
    dialogBox += '<select id="crime_modal_period" class="round_border">';
    dialogBox += '<option>Período do Dia</option>';
    dialogBox += '<option value="Morning">Manhã</option>';
    dialogBox += '<option value="Afternoon">Tarde</option>';
    dialogBox += '<option value="Evening">Noite</option>';
    dialogBox += '</select>';
    dialogBox += '</div>';
    dialogBox += '<div id="crime_modal_button">';
    dialogBox += '<button id="crime_modal_ok" class="round_border">Confirmar</button>';
    dialogBox += '<button id="crime_modal_cancel" class="round_border">Cancelar</button>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    document.getElementById("modal").innerHTML += dialogBox;
    document.getElementById('crime_modal_month').getElementsByTagName('option')[today.getMonth() + 1].selected = 'selected';

    var crimeModal = document.getElementById("crime_modal");
    var okButton = document.getElementById("crime_modal_ok");
    var cancelButton = document.getElementById("crime_modal_cancel");
    var description = document.getElementById("crime_modal_description");
    var month = document.getElementById("crime_modal_month");
    var day = document.getElementById("crime_modal_day");
    var year = document.getElementById("crime_modal_year");
    var period = document.getElementById("crime_modal_period");

    var self = this;
    okButton.onclick = function() {
        var crimeDate = new Date(year.value, month.value, day.value, 0, 0, 0, 0);
        self.shareCrime(location, description.value, crimeDate.getTime(), period.value);
        document.getElementById("modal").removeChild(crimeModal);
    };
    cancelButton.onclick = function() {
        self.share = false;
        document.getElementById("modal").removeChild(crimeModal);
    };
}

function daySelector(date) {

    var daySelector = '<select id="crime_modal_day" class="round_border">';
    daySelector += '<option>Dia</option>';
    for (var i = 1; i <= 31; i++) {
        if (date.getDate() === i) {
            daySelector += '<option value="' + i + '" selected>' + ("0" + i).slice(-2) + '</option>';
        } else {
            daySelector += '<option value="' + i + '">' + ("0" + i).slice(-2) + '</option>';
        }
    }
    daySelector += '</select>';

    return daySelector;
}

function monthSelector() {

    var monthSelector = '<select id="crime_modal_month" class="round_border">';
    monthSelector += '<option>Mês</option>';
    monthSelector += '<option value="0">Janeiro</option>';
    monthSelector += '<option value="1">Fevereiro</option>';
    monthSelector += '<option value="2">Março</option>';
    monthSelector += '<option value="3">Abril</option>';
    monthSelector += '<option value="4">Maio</option>';
    monthSelector += '<option value="5">Junho</option>';
    monthSelector += '<option value="6">Julho</option>';
    monthSelector += '<option value="7">Agosto</option>';
    monthSelector += '<option value="8">Setembro</option>';
    monthSelector += '<option value="9">Outubro</option>';
    monthSelector += '<option value="10">Novembro</option>';
    monthSelector += '<option value="11">Dezembro</option>';
    monthSelector += '</select>';

    return monthSelector;
}

function yearSelector(date) {

    var yearSelector = '<select id="crime_modal_year" class="round_border">';
    yearSelector += '<option>Ano</option>';
    for (var i = date.getFullYear(); i >= 2010; i--) {
        if (date.getFullYear() === i) {
            yearSelector += '<option value="' + i + '" selected>' + i + '</option>';
        } else {
            yearSelector += '<option value="' + i + '">' + i + '</option>';
        }
    }
    yearSelector += '</select>';

    return yearSelector;
}

/**
 Perfil Dialog Box 
 **/
function displayPerfilDialogBox(safetyAPI, callback) {

    var dialogBox = '<div id="perfil_modal" class="overlay">';
    dialogBox += '<div id="perfil_modal_container">';
    dialogBox += '<div id="perfil_modal_first_text">';
    dialogBox += '<span>Olá, é a primeira vez que você usa o Safety.<br>Ajude-nos a traçar seu perfil respondendo as perguntas abaixo.</span>';
    dialogBox += '</div>';
    dialogBox += '<div id="perfil_modal_options">';
    dialogBox += '<select id="perfil_modal_gender" class="round_border">';
    dialogBox += '<option>Identidade de Gênero</option>';
    dialogBox += '<option value="Female">Feminino</option>';
    dialogBox += '<option value="Fluid">Fluido</option>';
    dialogBox += '<option value="Male">Masculino</option>';
    dialogBox += '<option value="Transgender Female">Transgênero Feminino</option>';
    dialogBox += '<option value="Transgender Male">Transgênero Masculino</option>';
    dialogBox += '</select>';
    dialogBox += '<select id="perfil_modal_orientation" class="round_border">';
    dialogBox += '<option>Orientação Sexual</option>';
    dialogBox += '<option value="Asexual">Assexual</option>';
    dialogBox += '<option value="Bisexual">Bissexual</option>';
    dialogBox += '<option value="Heterosexual">Heterossexual</option>';
    dialogBox += '<option value="Homosexual">Homossexual</option>';
    dialogBox += '</select>';
    dialogBox += '<select id="perfil_modal_race" class="round_border">';
    dialogBox += '<option>Raça</option>';
    dialogBox += '<option value="Asian">Amarela</option>';
    dialogBox += '<option value="Caucasian">Branca</option>';
    dialogBox += '<option value="Indigene">Indígena</option>';
    dialogBox += '<option value="Black">Negra</option>';
    dialogBox += '<option value="Brown">Parda</option>';
    dialogBox += '</select>';
    dialogBox += ageSelector();
    dialogBox += '<button id="perfil_modal_ok" class="round_border">Confirmar</button>';
    dialogBox += '<button id="perfil_modal_cancel" class="round_border">Cancelar</button>';
    dialogBox += '</div>';
    dialogBox += '<div id="perfil_modal_second_text">';
    dialogBox += '<span>Nós não divulgamos dados dos nossos usuários.</span>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    document.getElementById("modal").innerHTML += dialogBox;

    var okButton = document.getElementById("perfil_modal_ok");
    var cancelButton = document.getElementById("perfil_modal_cancel");
    var perfilModal = document.getElementById("perfil_modal");
    var gender = document.getElementById("perfil_modal_gender");
    var orientation = document.getElementById("perfil_modal_orientation");
    var race = document.getElementById("perfil_modal_race");
    var age = document.getElementById("perfil_modal_age");

    okButton.onclick = function() {
        safetyAPI.createSession(gender.value, orientation.value, race.value, age.value, function(data, error) {
            callback(data, error);
        });
        document.getElementById("modal").removeChild(perfilModal);
    };
    cancelButton.onclick = function() {
        document.getElementById("modal").removeChild(perfilModal);
    };
}

function ageSelector() {

    var ageSelector = '<select id="perfil_modal_age" class="round_border">';
    ageSelector += '<option>Idade</option>';
    for (var i = 12; i <= 120; i++) {
        ageSelector += '<option value="' + i + '">' + i + '</option>';
    }
    ageSelector += '</select>';

    return ageSelector;
}

/**
 Crime Content Message Box 
 **/

function crimeContent(description, epoch, period) {

    var date = new Date(epoch);
    var content = '<div id="crime_content">';
    content += '<p>' + translateDescription(description) + '</p>';
    content += '<p>Quando: ' + ("0" + date.getDate()).slice(-2) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear() + '</p>';
    content += '<p>Período do Dia: ' + translatePeriod(period) + '</p>';
    content += '</div>';

    return content;
}

function translateDescription(description) {

    switch (description) {
        case "Robbery":
            return "Assalto";
            break;
        case "Attack":
            return "Agressão Física";
            break;
        case "Insult":
            return "Agressão Psicológica";
            break;
        case "Harassment":
            return "Assédio Sexual";
            break;
        case "Theft":
            return "Furto";
            break;
        case "Racism":
            return "Ódio Racial";
            break;
        case "Gender Violence":
            return "Violência de Gênero";
            break;
    }
}

function translatePeriod(period) {

    switch (period) {
        case "Morning":
            return "Manhã";
            break;
        case "Afternoon":
            return "Tarde";
            break;
        case "Evening":
            return "Noite";
            break;
    }
}

/**
 Route Dialog Box 
 **/

function displayAddressDialogBox(callback1, callback2, message) {

    var dialogBox = '<div id="route_modal" class="overlay">';
    dialogBox += '<div id="route_modal_div">';
    dialogBox += '<div id="route_modal_message">';
    dialogBox += message;
    dialogBox += '</div>';
    dialogBox += '<div id="route_modal_input">';
    dialogBox += '<input type="text" id="route_input" class="round_border" placeholder=" Digite um endereço">';
    dialogBox += '<button id="route_modal_choose" class="round_border">Mapa</button>';
    dialogBox += '</div>';
    dialogBox += '<div id="route_modal_button">';
    dialogBox += '<button id="route_modal_ok" class="round_border">Confirmar</button>';
    dialogBox += '<button id="route_modal_cancel" class="round_border">Cancelar</button>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    document.getElementById("modal").innerHTML += dialogBox;

    var routeModal = document.getElementById("route_modal");
    var chooseButton = document.getElementById("route_modal_choose");
    var okButton = document.getElementById("route_modal_ok");
    var cancelButton = document.getElementById("route_modal_cancel");

    chooseButton.onclick = function() {
        callback1 && callback1();
        document.getElementById("modal").removeChild(routeModal);
    };
    okButton.onclick = function() {
        callback2 && callback2(document.getElementById("route_input").value);
        document.getElementById("modal").removeChild(routeModal);
    };
    cancelButton.onclick = function() {
        document.getElementById("modal").removeChild(routeModal);
    };
}

function information(message){
    
    var dialogBox = '<div id="information_modal" class="overlay">';
    dialogBox += '<div id="information_modal_div">';
    dialogBox += '<div id="information_modal_message">';
    dialogBox += message;
    dialogBox += '</div>';
    dialogBox += '<div id="information_modal_button">';
    dialogBox += '<button id="information_ok" class="round_border">OK</button>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    dialogBox += '</div>';
    document.getElementById("modal").innerHTML += dialogBox;
 
    var informationModal = document.getElementById("information_modal");
    var okButton = document.getElementById("information_ok");
    
    okButton.onclick = function() {
        document.getElementById("modal").removeChild(informationModal);
    };
}