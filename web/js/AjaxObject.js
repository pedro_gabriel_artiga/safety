/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 Created on : Oct 23, 2014, 6:20:21 PM
 Author     : Pedro
 */

/**
 * Instantiate a new AjaxObject
 * @returns {AjaxObject}
 */
function AjaxObject() {
    this.xmlhttp = this.getXmlHttpRequestObject();
}

/**
 * @returns This function return a new XMLHttpRequest Object
 */
AjaxObject.prototype.getXmlHttpRequestObject = function() {
    if (window.XMLHttpRequest)
        return new XMLHttpRequest(); //For real browsers ...
    else
        return new ActiveXObject("Microsoft.XMLHTTP"); // For old IE versions ....
};

/**
 * 
 * @param {type} params
 * 
 * Params must be a JSON, int the following form:                                 
 * params = {
 *       method: "POST" or "GET",
 *       address: "IP_ADDRESS:PORT",
 *       resource: "resource, servlet, php, asp ...",
 *       async: true or false,
 *       data: {
 *           Another JSON containing input data for make a requisition ...
 *       }
 * };
 * 
 * @param {type} callback
 * 
 * Callback must be a function with the following signature:
 * function(data, error) {}
 * Data will containg the response from the Servlet.
 * Error should be null, but if samething went wrong will containg the error code.
 * 
 * @returns none
 */
AjaxObject.prototype.request = function(params, callback) {
    if (this.xmlhttp) {

        var protocol = location.protocol === "http:" ? location.protocol : "http:";
        var host = location.host || params.address;
        var url = protocol + "//" + host + params.resource;
        var parameters = "data=" + JSON.stringify(params.data);

        if (params.method === "POST") {
            this.xmlhttp.open(params.method, url, params.async);
            this.xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            this.xmlhttp.send(parameters);
        } else if (params.method === "GET") {
            url = url + "?" + parameters;
            this.xmlhttp.open(params.method, url, params.async);
            this.xmlhttp.send();
        }

        var self = this;
        this.xmlhttp.onreadystatechange = function() {
            if (self.xmlhttp.readyState === 4) {
                if (self.xmlhttp.status === 200) {
                    var jsonOutput = JSON.parse(self.xmlhttp.responseText);
                    
                    //Dealing with redirect ....
                    if(jsonOutput.hasOwnProperty('redirect')){
                        window.location = jsonOutput['redirect'];
                    }
                    
                    callback(jsonOutput, null);
                }
                else {
                    var errorCode = self.xmlhttp.status;
                    callback(null, errorCode);
                }
            }
        };
    }
};