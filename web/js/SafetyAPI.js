/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
 Created on : Nov 01, 2014, 6:20:21 PM
 Author     : Pedro
 */

function SafetyAPI() {
    this.ajax = new AjaxObject();
    this.db = new WebStorage();
    this.address = "54.207.39.246:8080";
}

SafetyAPI.prototype.requestSession = function(callback) {

    var self = this;
    //Try to read user ID from WebSQL
    this.db.readId(function(data, error) {
        if (error) {
            console.log("WebSQL error: " + error);
            callback(null, error);
        } else {
            var ajaxParams = {
                method: "POST",
                address: self.address,
                resource: "/Safety/Authentication",
                async: true,
                data: {
                    perfilId: data.perfilId
                }
            };

            //Ajax Request
            self.ajax.request(ajaxParams, function(data, error) {
                if (error) {
                    alert("Ajax Calling error: " + error);
                    callback(null, error);
                } else {
                    if (data.result === "success") {
                        callback(data);
                    } else {
                        callback(null, "SafetyAPI: Invalid ID");
                    }
                }
            });
        }
    });
};

SafetyAPI.prototype.createSession = function(gender, orientation, race, age, callback) {

    var ajaxParams = {
        method: "POST",
        address: this.address,
        resource: "/Safety/Authentication",
        async: true,
        data: {
            gender: gender,
            orientation: orientation,
            race: race,
            age: age
        }
    };

    //Ajax Request
    var self = this;
    this.ajax.request(ajaxParams, function(data, error) {
        if (error) {
            alert("Ajax Calling error: " + error);
            callback(null, error);
        } else {
            if (data.result === "success") {
                self.db.writeId(data, function(data, error) {
                    if (data) {
                        callback(data, null);
                    } else if (error) {
                        callback(null, error);
                    }
                });
            } else {
                callback(null, "Safety API: Error registering new Id");
            }
        }
    });
};

SafetyAPI.prototype.uploadCrime = function(latitude, longitude, description, epoch, period, callback) {

    var ajaxParams = {
        method: "POST",
        address: this.address,
        resource: "/Safety/CrimeRegister",
        async: true,
        data: {
            description: description,
            epoch: epoch,
            period: period,
            latitude: latitude,
            longitude: longitude
        }
    };

    //Ajax Request
    this.ajax.request(ajaxParams, function(data, error) {
        if (error) {
            alert("Ajax Calling error: " + error);
        } else {
            callback(data);
        }
    });
};

SafetyAPI.prototype.requestCrime = function(latitude, longitude, radius, callback) {

    var ajaxParams = {
        method: "POST",
        address: this.address,
        resource: "/Safety/CrimeList",
        async: true,
        data: {
            latitude: latitude,
            longitude: longitude,
            radius: radius,
            time: new Date().getHours()
        }
    };

    //Ajax Request
    this.ajax.request(ajaxParams, function(data, error) {
        if (error) {
            alert("Ajax Calling error: " + error);
        } else {
            callback(data);
        }
    });
};

SafetyAPI.prototype.selectRoute = function(originLat, originLon, destinationLat, destinationLon, callback) {

    var ajaxParams = {
        method: "POST",
        address: this.address,
        resource: "/Safety/RouteSelector",
        async: true,
        data: {
            originLat: originLat,
            originLon: originLon,
            destinationLat: destinationLat,
            destinationLon: destinationLon,
            time: new Date().getHours()
        }
    };

    //Ajax Request
    this.ajax.request(ajaxParams, function(data, error) {
        if (error) {
            alert("Ajax Calling error: " + error);
        } else {
            callback(data);
        }
    });
};