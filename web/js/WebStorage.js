/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function WebStorage() {
    this.db = openDatabase("Safety", "1.0", "Safety Web SQL Database", 50000);
}

WebStorage.prototype.readId = function(callback) {

    this.db.transaction(function(transaction) {
        transaction.executeSql('CREATE TABLE IF NOT EXISTS ID (id unique)');
        transaction.executeSql(
                "SELECT id FROM ID",
                [],
                function(transaction, result) {
                    if (result.rows.length === 1) {
                        callback({perfilId: result.rows.item(0)[['id']]}, null);
                    } else {
                        callback(null, "Query Failed");
                    }
                },
                function(transaction, error) {
                    callback(null, error);
                }
        );
    });
};

WebStorage.prototype.writeId = function(data, callback) {

    this.db.transaction(function(transaction) {
        transaction.executeSql('CREATE TABLE IF NOT EXISTS ID (id unique)');
        transaction.executeSql('DELETE FROM ID');
        transaction.executeSql(
                "INSERT INTO ID (id) VALUES (?)",
                [data.perfilId],
                function(transaction, result) {
                    callback(data, null);
                },
                function(transaction, error) {
                    callback(null, error);
                });
    });
};