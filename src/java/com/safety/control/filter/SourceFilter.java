/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.control.filter;

import java.io.IOException;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
public class SourceFilter implements Filter {

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader("Access-Control-Allow-Origin", "*");
        
        String resource = httpRequest.getServletPath();

        HttpSession session = httpRequest.getSession(false);
        boolean authetication;
        try {
            authetication = (boolean) session.getAttribute("userAuthentication");
        } catch (NullPointerException e) {
            authetication = false;
        }

        if (resource.equals("/CrimeRegister") || resource.equals("/CrimeList") || resource.equals("/RouteSelector")) {
            if (authetication) {
                chain.doFilter(request, response);
            } else {
                //Create a JsonObject to responde ...
                JsonObject jsonResponse = Json.createObjectBuilder()
                        .add("redirect", "../index.html")
                        .build();

                //Send the response to the ajax 
                httpResponse.setContentType("text/plain");
                httpResponse.setCharacterEncoding("UTF-8");
                httpResponse.getWriter().write(jsonResponse.toString());
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {

    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {

    }
}
