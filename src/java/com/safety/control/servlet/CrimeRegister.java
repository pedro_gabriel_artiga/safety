/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.control.servlet;

import com.safety.model.GeoPoint;
import com.safety.model.constants.CrimeDescription;
import com.safety.model.constants.CrimePeriod;
import com.safety.model.persistence.CrimeDAO;
import com.safety.util.Enum;
import com.safety.util.Logger;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
public class CrimeRegister extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Logger.logInfo("Connection received from: " + request.getRemoteHost());

        //Get session
        HttpSession session = request.getSession(false);
        Integer perfilId = (Integer) session.getAttribute("perfilId");
        HashSet<Integer> pulledIds = (HashSet<Integer>) session.getAttribute("pulledIds");

        int crimeId = 0;

        try {
            //Receive a JSON as String
            String jsonAsString = request.getParameter("data");
            //Create a JsonReader using the JsonAsString
            JsonReader jsonReader = Json.createReader(new StringReader(jsonAsString));
            //Create a JsonObject using the JsonReader
            JsonObject json = jsonReader.readObject();

            //Parse Json into variables ...
            String description = json.getString("description");
            Long epoch = Long.parseLong(json.get("epoch").toString());
            String period = json.getString("period");
            Double latitude = Double.parseDouble(json.get("latitude").toString());
            Double longitude = Double.parseDouble(json.get("longitude").toString());

            //New DAO Object
            CrimeDAO crime = new CrimeDAO();

            //Set values
            crime.setPerfilId(perfilId);
            crime.setDescription(Enum.check(CrimeDescription.values(), description));
            crime.setEpoch(epoch);
            crime.setPeriod(Enum.check(CrimePeriod.values(), period));
            crime.setPosition(new GeoPoint(latitude, longitude));

            //Save in the Database
            crimeId = crime.insert();
        } catch (Exception e) {
            Logger.logError(e);
        } finally {
            if (crimeId != 0) {
                pulledIds.add(crimeId);
            }
            //Define the result
            String result = (crimeId != 0) ? "success" : "failed";

            //Create a JsonObject to responde ...
            JsonObject jsonResponse = Json.createObjectBuilder()
                    .add("result", result)
                    .add("crimeId", crimeId)
                    .build();

            //Send the response to the ajax 
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonResponse.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
