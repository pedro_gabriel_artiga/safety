/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.control.servlet;

import com.safety.model.constants.Gender;
import com.safety.model.constants.Race;
import com.safety.model.constants.SexualOrientation;
import com.safety.model.persistence.PerfilDAO;
import com.safety.util.Enum;
import com.safety.util.Logger;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
public class Authentication extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Logger.logInfo("Authentication user: " + request.getRemoteHost());

        int perfilId = 0;
        try {

            //Receive a JSON as String
            String jsonAsString = request.getParameter("data");
            //Create a JsonReader using the JsonAsString
            JsonReader jsonReader = Json.createReader(new StringReader(jsonAsString));
            //Create a JsonObject using the JsonReader
            JsonObject json = jsonReader.readObject();

            PerfilDAO perfil = new PerfilDAO();

            //Parse Json into variables ...
            try {
                Integer id = Integer.parseInt(json.get("perfilId").toString());
                perfil.setId(id);
                if (perfil.select()) {
                    perfilId = perfil.getId();
                }
            } catch (Exception e) {

                Logger.logError(e);

                String gender = json.getString("gender");
                String orientation = json.getString("orientation");
                String race = json.getString("race");
                Integer age = Integer.parseInt(json.getString("age"));

                perfil.setGender(Enum.check(Gender.values(), gender));
                perfil.setOrientation(Enum.check(SexualOrientation.values(), orientation));
                perfil.setRace(Enum.check(Race.values(), race));
                perfil.setAge(age);

                perfilId = perfil.insert();
            }

        } catch (Exception e) {
            Logger.logError(e);
        } finally {

            //Define the result
            String result = (perfilId != 0) ? "success" : "failed";

            if (result.equals("success")) {
                //Do stuff
                HttpSession session = request.getSession(true);
                session.setMaxInactiveInterval(3600 * 24);
                session.setAttribute("userAuthentication", true);
                session.setAttribute("perfilId", perfilId);
                session.setAttribute("pulledIds", new HashSet<Integer>());
            }

            //Create a JsonObject to responde ...
            JsonObject jsonResponse = Json.createObjectBuilder()
                    .add("result", result)
                    .add("perfilId", perfilId)
                    .build();

            //Send the response to the ajax 
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonResponse.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
