/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.control.servlet;

import com.safety.model.Crime;
import com.safety.model.GeoPoint;
import com.safety.model.Path;
import com.safety.model.persistence.CrimeDAO;
import com.safety.util.Logger;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pedro
 */
public class RouteSelector extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Create a JsonArrayBuilder
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        int wayPointsNumber = 0;

        try {
            //Receive a JSON as String
            String jsonAsString = request.getParameter("data");
            //Create a JsonReader using the JsonAsString
            JsonReader jsonReader = Json.createReader(new StringReader(jsonAsString));
            //Create a JsonObject using the JsonReader
            JsonObject json = jsonReader.readObject();

            //Parse time
            int time = Integer.parseInt(json.get("time").toString());

            //Parse Json into variables ...  
            GeoPoint origin = new GeoPoint(
                    Double.parseDouble(json.get("originLat").toString()),
                    Double.parseDouble(json.get("originLon").toString())
            );

            GeoPoint destination = new GeoPoint(
                    Double.parseDouble(json.get("destinationLat").toString()),
                    Double.parseDouble(json.get("destinationLon").toString())
            );

            Logger.logInfo("Calculating route from: " + origin + " to: " + destination);

            List<Crime> crimes = new CrimeDAO().selectList(origin.getLatitude(), origin.getLongitude(), 0.1, time);

            Path path = new Path(origin, destination, crimes);
            long startTime = System.nanoTime();
            List<GeoPoint> route = path.calculatePath();
            long elapsedTime = System.nanoTime() - startTime;

            Logger.logInfo("Elapsed Time: " + elapsedTime / 1000000l);

            //Calculate number of waypoints based on distance
            double distance = origin.calculateDistanceTo(destination);

            if (distance <= 0.8) {
                wayPointsNumber = 5;
            } else if (distance > 0.8 && distance <= 1.6) {
                wayPointsNumber = 6;
            } else if (distance > 1.6 && distance <= 2.4) {
                wayPointsNumber = 7;
            } else if (distance > 2.4 && distance <= 3.2) {
                wayPointsNumber = 8;
            } else if (distance > 3.2) {
                wayPointsNumber = 9;
            }

            //Select waypoints
            double max = Math.floor(route.size() / wayPointsNumber);
            List<GeoPoint> wayPoints = new ArrayList();
            for (int i = 0; i < route.size(); i++) {
                if (i != 0 && i % max == 0 && wayPoints.size() < wayPointsNumber - 1) {
                    wayPoints.add(route.get(i));
                }
            }

            for (GeoPoint point : wayPoints) {
                jsonArrayBuilder.add(Json.createObjectBuilder()
                        .add("longitude", point.getLongitude())
                        .add("latitude", point.getLatitude())
                );
            }

        } catch (Exception e) {
            Logger.logError(e);
        } finally {

            //Create a JsonObject to responde ...
            JsonObject jsonResponse = Json.createObjectBuilder()
                    .add("points", jsonArrayBuilder)
                    .add("nWay", wayPointsNumber)
                    .build();

            //Send the response to the ajax 
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonResponse.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
