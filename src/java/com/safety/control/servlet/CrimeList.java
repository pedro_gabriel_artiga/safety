/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.control.servlet;

import com.safety.model.Crime;
import com.safety.model.GeoPoint;
import com.safety.model.persistence.CrimeDAO;
import com.safety.util.Logger;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Pedro
 */
public class CrimeList extends HttpServlet {

    private static final double DELTA = 1.0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Logger.logInfo("Connection received from: " + request.getRemoteHost());

        //Get session
        HttpSession session = request.getSession(false);
        Set<Integer> pulledIds = (Set<Integer>) session.getAttribute("pulledIds");

        //Create a JsonArrayBuilder
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();

        try {
            //Receive a JSON as String
            String jsonAsString = request.getParameter("data");
            //Create a JsonReader using the JsonAsString
            JsonReader jsonReader = Json.createReader(new StringReader(jsonAsString));
            //Create a JsonObject using the JsonReader
            JsonObject json = jsonReader.readObject();

            //Parse Hour
            int hour = Integer.parseInt(json.get("time").toString());

            //Parse Latitude and Longitude as Radians
            Double latitude = Double.parseDouble(json.get("latitude").toString());
            Double longitude = Double.parseDouble(json.get("longitude").toString());

            //Check if radius is between 0.0 and 10.0
            Double radius = Double.parseDouble(json.get("radius").toString());
            radius = (radius >= 0.0) ? radius : 0.0;

            GeoPoint position = new GeoPoint(latitude, longitude);

            //New DAO Object
            CrimeDAO crime = new CrimeDAO();

            //Search in the Database
            List<Crime> crimes = crime.selectList(latitude, longitude, DELTA, hour);

            for (Crime cr : crimes) {
                if (!pulledIds.contains(cr.getId())) {

                    double distance = position.calculateDistanceTo(cr.getPosition());

                    if (distance <= radius) {
                        pulledIds.add(cr.getId());
                        jsonArrayBuilder.add(Json.createObjectBuilder()
                                .add("crimeId", cr.getId())
                                .add("description", cr.getDescription().getValue())
                                .add("epoch", cr.getEpoch())
                                .add("period", cr.getPeriod().getValue())
                                .add("latitude", cr.getPosition().getLatitude())
                                .add("longitude", cr.getPosition().getLongitude())
                        );
                        Logger.logInfo("Distance km: " + distance);
                    }
                }
            }
        } catch (Exception e) {
            Logger.logError(e);
        } finally {
            //Create a JsonObject to responde ...
            JsonObject jsonResponse = Json.createObjectBuilder().add("points", jsonArrayBuilder).build();

            //Send the response to the ajax 
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(jsonResponse.toString());
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
