/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.util;

import com.safety.model.constants.IEnum;

/**
 *
 * @author Pedro
 */
public class Enum {

    public static IEnum check(IEnum[] values, String description) {

        for (IEnum enumeration : values) {
            if (enumeration.getValue().equals(description)) {
                return enumeration;
            }
        }
        throw new IllegalArgumentException("No enum constant: " + description);
    }
}
