/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.util;

/**
 *
 * @author Pedro
 */
public class Logger {

    public static void logInfo(Object object) {
        System.out.println(object);
    }
    
    public static void logInfo(String message) {
        System.out.println(message);
    }

    public static void logError(String message) {
        System.err.println(message);
    }
    
    public static void logError(Exception e) {
        StackTraceElement[] stack = e.getStackTrace();
        if (stack.length > 0) {
            System.err.println("Source\\File: " + stack[ 0].getFileName());
            System.err.println("Line: " + stack[ 0].getLineNumber());
            System.err.println("Message: " + e.getMessage());
            System.err.println("Exception: " + e);
        }
    }
}
