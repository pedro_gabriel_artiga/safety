/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model;

/**
 *
 * @author Pedro
 */
public class GeoPoint {

    private static final int EARTH_RADIUS = 6373;

    private double latitude;
    private double longitude;
    private double latAsRadians;
    private double lonAsRadians;
    private double latCosine;

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public GeoPoint() {
    }

    public GeoPoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;

        this.lonAsRadians = Math.toRadians(this.longitude);
        this.latAsRadians = Math.toRadians(this.latitude);
        this.latCosine = Math.cos(this.latAsRadians);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        if (latitude >= -90.0 && latitude <= 90.00) {
            this.latitude = latitude;
        }
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        if (longitude >= -180.0 && longitude <= 180.0) {
            this.longitude = longitude;
        }
    }

    public double getLatAsRadians() {
        return latAsRadians;
    }

    public void setLatAsRadians(double latAsRadians) {
        if (latAsRadians >= (-Math.PI / 2.0) && latAsRadians <= (Math.PI / 2.0)) {
            this.latAsRadians = latAsRadians;
        }
    }

    public double getLonAsRadians() {
        return lonAsRadians;
    }

    public void setLonAsRadians(double lonAsRadians) {
        if (lonAsRadians >= -Math.PI && lonAsRadians <= Math.PI) {
            this.lonAsRadians = lonAsRadians;
        }
    }

    public double getLatCosine() {
        return latCosine;
    }

    public void setLatCosine(double latCosine) {
        if (latCosine >= -1.0 && latCosine <= 1.0) {
            this.latCosine = latCosine;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Methods">
    /**
     * Calculate the earth-distance between this GeoPoint and a target GeoPoint.
     *
     * @param target
     * @return Earth distance in Km as a double.
     */
    public double calculateDistanceTo(GeoPoint target) {

        double deltaLat = target.getLatAsRadians() - this.latAsRadians;
        double deltaLon = target.getLonAsRadians() - this.lonAsRadians;

        double a = Math.pow(Math.sin(deltaLat / 2.0), 2) + target.getLatCosine()
                * Math.cos(this.latAsRadians) * Math.pow(Math.sin(deltaLon / 2.0), 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return EARTH_RADIUS * c; //distance of two points on earth in Km
    }

    public boolean similarTo(GeoPoint target, double tolerance) {
        return (this.latitude <= target.getLatitude() + tolerance && this.latitude >= target.getLatitude() - tolerance)
                && (this.longitude <= target.getLongitude() + tolerance && this.longitude >= target.getLongitude() - tolerance);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Overriders">
    @Override
    public String toString() {
        return "(" + longitude + ", " + latitude + ')';
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || this.getClass() != object.getClass()) {
            return false;
        }
        final GeoPoint other = (GeoPoint) object;
        if (Double.doubleToLongBits(this.longitude) != Double.doubleToLongBits(other.getLongitude())) {
            return false;
        }
        return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(other.getLatitude());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        return hash;
    }
    // </editor-fold>
}
