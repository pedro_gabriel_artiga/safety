/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Pedro
 */
class ControlLoop {

    private static final long TIME_THRESHOLD = 2000000000l;
    private static final long TIMEOUT = 10000000000l;

    private static final double DANGER_RADIUS = 0.2;
    private static final double DANGER_WEIGHT = 10.0;
    private static final double DISTANCE_WEIGHT = 1.0;

    private final Path path;
    private long startTime;

    ControlLoop(Path path) {
        this.path = path;
        this.path.setDangerRadius(DANGER_RADIUS);
        this.path.setDangerWeight(DANGER_WEIGHT);
        this.path.setDistanceWeight(DISTANCE_WEIGHT);
    }

    void start() {
        this.startTime = System.nanoTime();
    }

    boolean decide() {
        long elapsedTime = System.nanoTime() - this.startTime;

        if (TIMEOUT - elapsedTime < 0) {
            return false;
        }
        if (TIME_THRESHOLD - elapsedTime < 0) {
            double times = elapsedTime / TIME_THRESHOLD;
            this.path.setDangerRadius(DANGER_RADIUS / times);
            this.path.setDangerWeight(DANGER_WEIGHT / times);
        }
        return true;
    }
}

public class Path {

    private static final double TOLERANCE = 0.0001;
    private static final double STEP = 50.0;

    private final GeoPoint origin;
    private final GeoPoint destination;
    private final List<Crime> crimes;
    private final ControlLoop controlLoop;

    private final double maxDistance;

    private double dangerRadius;
    private double dangerWeight;
    private double distanceWeight;
    private boolean crimeFlag;

    public Path(GeoPoint origin, GeoPoint destination, List<Crime> crimes) {
        this.origin = origin;
        this.destination = destination;
        this.crimes = crimes;

        this.maxDistance = this.origin.calculateDistanceTo(this.destination);
        this.controlLoop = new ControlLoop(this);

        this.crimeFlag = false;
    }

    private class PathStructure implements Comparable {

        private final GeoPoint node;
        private final double gCost;
        private final double hCost;
        private final double fCost;
        private final List<GeoPoint> path;

        public PathStructure(GeoPoint node, double gCost, double hCost, List<GeoPoint> path) {
            this.node = node;
            this.gCost = gCost;
            this.hCost = hCost;
            this.fCost = this.gCost + this.hCost;
            this.path = path;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + (int) (Double.doubleToLongBits(this.fCost) ^ (Double.doubleToLongBits(this.fCost) >>> 32));
            return hash;
        }

        @Override
        public boolean equals(Object object) {
            if (object == null || this.getClass() != object.getClass()) {
                return false;
            }
            final PathStructure other = (PathStructure) object;
            return Double.doubleToLongBits(this.fCost) == Double.doubleToLongBits(other.fCost);
        }

        @Override
        public int compareTo(Object object) {

            if (object == null) {
                throw new NullPointerException();
            }
            if (getClass() != object.getClass()) {
                throw new ClassCastException();
            }
            final PathStructure other = (PathStructure) object;
            return Double.compare(this.fCost, other.fCost);
        }
    }

    private double dangerLevel(GeoPoint node) {

        double dangerLevel = 0.0;
        int nCrimes = 0;
        for (Crime cr : this.crimes) {
            double distance = node.calculateDistanceTo(cr.getPosition());
            if (distance < dangerRadius) {
                dangerLevel += (dangerRadius - distance) / dangerRadius;
                nCrimes++;
            }
        }

        if (!this.crimeFlag && nCrimes != 0) {
            this.crimeFlag = true;
        }

        return nCrimes != 0 ? dangerLevel / nCrimes : 0.0;
    }

    private double heuristic(GeoPoint node) {

        double distanceCost = node.calculateDistanceTo(this.destination);
        distanceCost = distanceCost / this.maxDistance;

        return distanceWeight * distanceCost + dangerWeight * this.dangerLevel(node);
    }

    private List<GeoPoint> neighbors(GeoPoint node, double stepLon, double stepLat) {

        List<GeoPoint> neighbors = new ArrayList();

        neighbors.add(new GeoPoint(node.getLatitude() + stepLat, node.getLongitude()));
        neighbors.add(new GeoPoint(node.getLatitude() - stepLat, node.getLongitude()));
        neighbors.add(new GeoPoint(node.getLatitude(), node.getLongitude() - stepLon));
        neighbors.add(new GeoPoint(node.getLatitude(), node.getLongitude() + stepLon));

        return neighbors;
    }

    public List<GeoPoint> calculatePath() {

        if (this.crimes.isEmpty()) {
            return Collections.emptyList();
        }

        this.controlLoop.start();

        double stepLon = Math.abs(this.destination.getLongitude() - this.origin.getLongitude()) / STEP;
        double stepLat = Math.abs(this.destination.getLatitude() - this.origin.getLatitude()) / STEP;

        Set<GeoPoint> closed = new HashSet();
        List<PathStructure> open = new ArrayList();

        open.add(new PathStructure(
                this.origin,
                0,
                this.heuristic(this.origin),
                new ArrayList(Arrays.asList(this.origin)))
        );

        while (!open.isEmpty()) {

            PathStructure current = open.remove(0);

            GeoPoint node = current.node;
            double gCost = current.gCost;
            List<GeoPoint> path = current.path;

            if (closed.add(node)) {
                if (node.similarTo(this.destination, TOLERANCE)) {
                    if (this.crimeFlag) {
                        return path;
                    } else {
                        return Collections.emptyList();
                    }
                } else {
                    for (GeoPoint neighbor : neighbors(node, stepLon, stepLat)) {

                        double neighborG = gCost + (node.calculateDistanceTo(neighbor) / this.maxDistance);
                        double neighborH = this.heuristic(neighbor);
                        List<GeoPoint> neighborPath = new ArrayList(path);
                        neighborPath.add(neighbor);
                        open.add(new PathStructure(neighbor, neighborG, neighborH, neighborPath));
                    }
                    Collections.sort(open);
                }
            }

            /**
             * Check elapsed time and decide for a action.
             */
            if (!this.controlLoop.decide()) {
                return Collections.emptyList();
            }
        }
        return Collections.emptyList();
    }

    public double getDangerRadius() {
        return dangerRadius;
    }

    public void setDangerRadius(double dangerRadius) {
        this.dangerRadius = dangerRadius;
    }

    public double getDangerWeight() {
        return dangerWeight;
    }

    public void setDangerWeight(double dangerWeight) {
        this.dangerWeight = dangerWeight;
    }

    public double getDistanceWeight() {
        return distanceWeight;
    }

    public void setDistanceWeight(double distanceWeight) {
        this.distanceWeight = distanceWeight;
    }
}
