/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.constants;

/**
 *
 * @author Pedro
 */
public enum CrimeDescription implements IEnum {

    ROBBERY("Robbery"), THEFT("Theft"), ATTACK("Attack"), INSULT("Insult"),
    HARASSMENT("Harassment"), RACISM("Racism"), GENDER("Gender Violence");

    // Internal state
    private final String value;

    // Constructor
    private CrimeDescription(String value) {
        this.value = value;
    }

    // Return Enum Value
    @Override
    public String getValue() {
        return value;
    }
}
