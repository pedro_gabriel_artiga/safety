/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.constants;

/**
 *
 * @author Pedro
 */
public enum Race implements IEnum {

    ASIAN("Asian"), CAUCASIAN("Caucasian"), INDIGENE("Indigene"), 
    BLACK("Black"), BROWN("Brown");

    // Internal state
    private final String value;

    // Constructor
    private Race(String value) {
        this.value = value;
    }

    // Return Enum Value
    @Override
    public String getValue() {
        return value;
    }
}
