/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.constants;

/**
 *
 * @author Pedro
 */
public enum Gender implements IEnum {

    FEMALE("Female"), MALE("Male"), TRANSGENDER_FEMALE("Transgender Female"), 
    TRANSGENDER_MALE("Transgender Male"), FLUID("Fluid");

    // Internal state
    private final String value;

    // Constructor
    private Gender(String value) {
        this.value = value;
    }

    // Return Enum Value
    @Override
    public String getValue() {
        return value;
    }
}
