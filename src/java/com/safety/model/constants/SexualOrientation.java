/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.constants;

/**
 *
 * @author Pedro
 */
public enum SexualOrientation implements IEnum {

    HETEROSEXUAL("Heterosexual"), HOMOSEXUAL("Homosexual"),
    BISEXUAL("Bisexual"), ASEXUAL("Asexual");

    // Internal state
    private final String value;

    // Constructor
    private SexualOrientation(String value) {
        this.value = value;
    }

    // Return Enum Value
    @Override
    public String getValue() {
        return value;
    }
}
