/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.persistence;

import com.safety.model.Crime;
import com.safety.model.GeoPoint;
import com.safety.model.constants.CrimeDescription;
import com.safety.model.constants.CrimePeriod;
import com.safety.util.Enum;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pedro
 */
public class CrimeDAO extends Crime {

    private final ConnectionJavaSQL connection;

    public CrimeDAO() {
        connection = new ConnectionJavaSQL();
    }

    /**
     * Insert the Crime into the Database.
     *
     * @return rowId(auto_increment) if insertion works, zero otherwise
     */
    public int insert() {

        String sql
                = "insert into Crime(perfilId, description, period, epoch, latitude, longitude, latAsRadians, lonAsRadians, latCosine)"
                + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        return connection.executeCommand(
                sql,
                super.getPerfilId(),
                super.getDescription().getValue(),
                super.getPeriod().getValue(),
                super.getEpoch(),
                super.getPosition().getLatitude(),
                super.getPosition().getLongitude(),
                super.getPosition().getLatAsRadians(),
                super.getPosition().getLonAsRadians(),
                super.getPosition().getLatCosine()
        );
    }

    /**
     * Select crimes from the database.
     *
     * @param latitude
     * @param longitude
     * @param delta
     * @param time
     * @return crimes list
     */
    public List<Crime> selectList(double latitude, double longitude, double delta, int time) {

        List<Crime> crimes = new ArrayList();

        String period;
        if (time >= 5 && time < 12) {
            period = "Morning";
        } else if (time >= 12 && time < 18) {
            period = "Afternoon";
        } else {
            period = "Evening";
        }

        String sql = "select * from Crime where (latitude >= ? and latitude <= ?) and (longitude >= ? and longitude <= ?) and period == ?";
        List<Map> mapList = connection.executeQuery(
                sql, latitude - delta, latitude + delta, longitude - delta, longitude + delta, period
        );

        if (mapList != null) {
            Crime crime;
            GeoPoint position;
            for (Map map : mapList) {
                crime = new Crime();
                position = new GeoPoint();

                crime.setId((Integer) map.get("id"));
                crime.setPerfilId((Integer) map.get("perfilId"));
                crime.setDescription(Enum.check(CrimeDescription.values(), (String) map.get("description")));
                crime.setEpoch((Long) map.get("epoch"));
                crime.setPeriod(Enum.check(CrimePeriod.values(), (String) map.get("period")));
                position.setLatitude((Double) map.get("latitude"));
                position.setLongitude((Double) map.get("longitude"));
                position.setLatAsRadians((Double) map.get("latAsRadians"));
                position.setLonAsRadians((Double) map.get("lonAsRadians"));
                position.setLatCosine((Double) map.get("latCosine"));
                crime.setPosition(position);

                crimes.add(crime);
            }
            return crimes;
        } else {
            return Collections.emptyList();
        }
    }
}
