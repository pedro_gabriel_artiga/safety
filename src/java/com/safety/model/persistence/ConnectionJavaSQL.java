/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.persistence;

import com.safety.util.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pedro
 */
final class ConnectionJavaSQL {

    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String URL = "jdbc:sqlite:C:/sqlite/safety";
    private static final String USER = "";
    private static final String PASSWORD = "";

    /**
     * Start a connection with the database.
     *
     * @return a connection or exit.
     */
    private Connection getConnection() {

        Connection connection = null;

        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (ClassNotFoundException e) {
            System.err.println("\n\nUnknow Driver.");
            System.exit(-1);
        } catch (SQLException e) {
            Logger.logError(e);
        }

        return connection;
    }

    /**
     * Execute a SQL command into database (insert, update, delete)
     *
     * @param sqlClause - "insert into Table(column1, column2, column3, column4)
     * values (?, ?, ?, ?)"
     * @param values - The SQL parameters
     * @return RowId if ok, Zero otherwise
     */
    protected int executeCommand(String sqlClause, Object... values) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet keys = null;

        try {
            connection = getConnection();
            preparedStatement = connection.prepareStatement(sqlClause, Statement.RETURN_GENERATED_KEYS);

            for (int i = 0; i < values.length; i++) {
                preparedStatement.setObject(i + 1, values[i]);
            }

            preparedStatement.executeUpdate();
            keys = preparedStatement.getGeneratedKeys();
            keys.next();
            return keys.getInt(1);
        } catch (SQLException e) {
            Logger.logError(e);
            return 0;
        } finally {
            try {
                if (keys != null) {
                    keys.close();
                }

                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                Logger.logError(e);
            }
        }
    }

    /**
     * Execute a SQL query into database (select)
     *
     * @param sqlClause - "select * from Table where column1 = ? and column3 =
     * ?";
     * @param values - the parameters
     * @return A List of Maps, where it Map represents a row and it Map value a
     * column.
     */
    protected List<Map> executeQuery(String sqlClause, Object... values) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            //Start new connection
            connection = getConnection();
            //Create a preparedStatement using the sql clause
            preparedStatement = connection.prepareStatement(sqlClause);

            //Set each paramenter into statement
            for (int i = 0; i < values.length; i++) {
                preparedStatement.setObject(i + 1, values[i]);
            }

            //Execute query
            resultSet = preparedStatement.executeQuery();

            //Get the number of columns in the result
            ResultSetMetaData rsmd = resultSet.getMetaData();
            int columnCount = rsmd.getColumnCount();

            String columnName;
            Map map;

            //Create a new list of Map (rows)
            List<Map> results = new ArrayList<>();

            //For each row
            while (resultSet.next()) {

                //create a new Map
                map = new HashMap();

                //For each column 
                for (int i = 1; i <= columnCount; i++) {
                    //Add a object into the Map, using the column name as key.
                    columnName = rsmd.getColumnName(i);
                    map.put(columnName, resultSet.getObject(columnName));
                }

                //Add the map "row" into the list of rows (result)
                results.add(map);
            }

            //Finaly return the list containing all rows
            return results;
        } catch (SQLException | NullPointerException e) {
            Logger.logError(e);
            return null;
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }

                if (preparedStatement != null) {
                    preparedStatement.close();
                }

                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                Logger.logError(e);
            }
        }
    }
}
