/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model.persistence;

import com.safety.model.Perfil;
import com.safety.model.constants.Gender;
import com.safety.model.constants.Race;
import com.safety.model.constants.SexualOrientation;
import com.safety.util.Enum;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Pedro
 */
public class PerfilDAO extends Perfil {

    private final ConnectionJavaSQL connection;

    public PerfilDAO() {
        connection = new ConnectionJavaSQL();
    }

    /**
     * Insert the Perfil into the Database.
     *
     * @return rowId(auto_increment) if insertion works, zero otherwise
     */
    public int insert() {

        String sql
                = "insert into Perfil(gender, orientation, race, age)"
                + " values (?, ?, ?, ?)";

        return connection.executeCommand(
                sql,
                super.getGender().getValue(),
                super.getOrientation().getValue(),
                super.getRace().getValue(),
                super.getAge()
        );
    }

    /**
     * Select Perfil by ID
     *
     * @return true if query works
     */
    public boolean select() {

        String sql = "select * from Perfil where id = ?";
        List<Map> mapList = connection.executeQuery(sql, this.getId());

        if (mapList != null && mapList.size() == 1) {

            super.setId((Integer) mapList.get(0).get("id"));
            super.setGender(Enum.check(Gender.values(), (String) mapList.get(0).get("gender")));
            super.setOrientation(Enum.check(SexualOrientation.values(), (String) mapList.get(0).get("orientation")));
            super.setRace(Enum.check(Race.values(), (String) mapList.get(0).get("race")));
            super.setAge((Integer) mapList.get(0).get("age"));

            return true;
        }
        return false;
    }
}
