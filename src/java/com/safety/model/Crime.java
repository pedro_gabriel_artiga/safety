/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model;

import com.safety.model.constants.IEnum;

/**
 *
 * @author Pedro
 */
public class Crime {

    private Integer id;
    private Integer perfilId;
    private IEnum description;
    private IEnum period;
    private Long epoch;
    private GeoPoint position;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (id != null && id >= 0) {
            this.id = id;
        }
    }

    public Integer getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(Integer perfilId) {
        if (perfilId != null && perfilId >= 0) {
            this.perfilId = perfilId;
        }
    }

    public IEnum getDescription() {
        return description;
    }

    public void setDescription(IEnum description) {
        if (description != null) {
            this.description = description;
        }
    }

    public IEnum getPeriod() {
        return period;
    }

    public void setPeriod(IEnum period) {
        if (period != null) {
            this.period = period;
        }
    }

    public Long getEpoch() {
        return epoch;
    }

    public void setEpoch(Long epoch) {
        if (epoch >= 0) {
            this.epoch = epoch;
        }
    }

    public GeoPoint getPosition() {
        return position;
    }

    public void setPosition(GeoPoint position) {
        this.position = position;
    }
}
