/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.safety.model;

import com.safety.model.constants.IEnum;

/**
 *
 * @author Pedro
 */
public class Perfil {

    private Integer id;
    private Integer age;
    private IEnum gender;
    private IEnum orientation;
    private IEnum race;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        if (id != null && id >= 0) {
            this.id = id;
        }
    }

    public IEnum getGender() {
        return gender;
    }

    public void setGender(IEnum gender) {
        if (gender != null) {
            this.gender = gender;
        }
    }

    public IEnum getOrientation() {
        return orientation;
    }

    public void setOrientation(IEnum orientation) {
        if (orientation != null) {
            this.orientation = orientation;
        }
    }

    public IEnum getRace() {
        return race;
    }

    public void setRace(IEnum race) {
        if (race != null) {
            this.race = race;
        }
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        if (age >= 12 && age <= 120) {
            this.age = age;
        }
    }
}
